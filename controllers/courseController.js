const Course = require("../models/Course");
const User = require("../models/User");

// Controller Functions:

module.exports.addCourse = (reqBody, userData) => {
  if (userData.isAdmin == true) {
    let newCourse = new Course({
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
    });

    return newCourse.save().then((course, error) => {
      if (error) {
        return false; //"Course creation failed."
      } else {
        return true;
      }
    });
  } else {
    return false;
  }
};

// Retrieveing All Courses - BEST PRACTICE
module.exports.getAllCourses = (data) => {
  if (data.isAdmin) {
    return Course.find({}).then((result) => {
      return result;
    });
  } else {
    return false; // "You are not an admin"
  }
};

// Retrieve All Active Courses
module.exports.getAllActive = () => {
  return Course.find({ isActive: true }).then((result) => {
    return result;
  });
};

// Retrieve a Specific Course
module.exports.getCourse = (reqParams) => {
  return Course.findById(reqParams.courseId).then((result) => {
    return result;
  });
};

// Update a Specific Course
module.exports.updateCourse = (reqParams, reqBody) => {
  let updatedCourse = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
    isActive: reqBody.isActive,
  };

  return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(
    (updatedCourse, error) => {
      console.log(updatedCourse);
      if (error) {
        return false;
      } else {
        return true;
      }
    }
  );
};

// Archive a Specific Course
module.exports.archiveCourse = (reqParams, reqBody) => {
  let archivedCourse = {
    isActive: reqBody.isActive,
  };

  return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then(
    (archivedCourse, error) => {
      console.log(archivedCourse);
      if (error) {
        return false;
      } else {
        return true;
      }
    }
  );
};

